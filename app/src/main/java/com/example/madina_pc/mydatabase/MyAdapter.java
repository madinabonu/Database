package com.example.madina_pc.mydatabase;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Madina-PC on 28.10.2017.
 */

public class MyAdapter extends BaseAdapter {
    private List<Model> list;
    private Model data;

    public MyAdapter(List<Model> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, viewGroup, false);
        TextView name = view.findViewById(R.id.name);
        TextView surname = view.findViewById(R.id.surname);
        TextView sex = view.findViewById(R.id.sex);
        TextView group = view.findViewById(R.id.group);
        TextView faculty = view.findViewById(R.id.faculty);
        TextView address = view.findViewById(R.id.address);
        TextView telephone = view.findViewById(R.id.telephone);

        data = list.get(i);
        name.setText(data.getName());
        surname.setText(data.getSurname());
        sex.setText(data.getSex());
        group.setText(data.getGroup());
        faculty.setText(data.getFaculty());
        address.setText(data.getAddress());
        telephone.setText(data.getTelephone());

        return view;
    }
}
