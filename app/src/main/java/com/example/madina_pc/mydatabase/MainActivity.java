package com.example.madina_pc.mydatabase;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    static final String DATABASE_NAME = "myBase";
    private SQLiteDatabase database;
    private ListView lv;
    private List<Model> data;
    private ArrayList<String> name;
    private ArrayList<String> surname;
    private ArrayList<String> sex;
    private ArrayList<String> group;
    private ArrayList<String> faculty;
    private ArrayList<String> address;
    private ArrayList<String> telephone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = findViewById(R.id.list);
        data = new ArrayList<>();
        name = new ArrayList<>();
        surname = new ArrayList<>();
        sex = new ArrayList<>();
        group = new ArrayList<>();
        faculty = new ArrayList<>();
        address = new ArrayList<>();
        telephone = new ArrayList<>();

        ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(this, DATABASE_NAME);
        database = dbOpenHelper.openDataBase();
        Log.i("madina", "ulanibdi");

        Cursor friendCursor = database.query("Students",
                new String[]
                        {"rowid", "Name", "Surname", "Sex", "Guruh", "Faculty", "Address", "Telephone"},
                null, null, null, null
                , null);
        friendCursor.moveToFirst();
        if (!friendCursor.isAfterLast()) {
            do {

                int x = friendCursor.getInt(0);
                String ism = friendCursor.getString(1);
                name.add(ism);
                String sur = friendCursor.getString(2);
                surname.add(sur);
                String gender = friendCursor.getString(3);
                sex.add(gender);
                String guruh = friendCursor.getString(4);
                group.add(guruh);
                String fac = friendCursor.getString(5);
                faculty.add(fac);
                String add = friendCursor.getString(6);
                address.add(add);
                String tel = friendCursor.getString(7);
                telephone.add(tel);


            } while (friendCursor.moveToNext());
        }
        friendCursor.close();


        for (int i = 0; i < 3; i++) {
            data.add(new Model(name.get(i), surname.get(i), sex.get(i), group.get(i), faculty.get(i), address.get(i), telephone.get(i)));
        }
        MyAdapter adapter = new MyAdapter(data);
        lv.setAdapter(adapter);

    }
}
