package com.example.madina_pc.mydatabase;

/**
 * Created by Madina-PC on 28.10.2017.
 */

public class Model {
    private String name;
    private String surname;
    private String sex;
    private String group;
    private String faculty;
    private String address;
    private String telephone;

    public Model(String name, String surname, String sex, String group, String faculty, String address, String telephone) {
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.group = group;
        this.faculty = faculty;
        this.address = address;
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSex() {
        return sex;
    }

    public String getGroup() {
        return group;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getAddress() {
        return address;
    }

    public String getTelephone() {
        return telephone;
    }
}
